﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GiftVotingSystem.Models
{
    public class GiftPollResult
    {
        public string GiftName { get; set; }
        public int GiftId { get; set; }
        public int VotesCount { get; set; }
        public List<string> Users { get; set; }
    }
}