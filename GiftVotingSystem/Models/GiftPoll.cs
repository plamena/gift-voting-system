﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GiftVotingSystem.Models
{
    public class GiftPoll
    {
        public int Id { get; set; }

        [Required]
        public string InitiatorId { get; set; }

        // the person that created the poll
        [ForeignKey("InitiatorId")]
        [InverseProperty("InitiatedGiftPolls")]
        public virtual ApplicationUser Initiator { get; set; }

        // uniqueness constraint - there can be only one poll per year for each user
        [Required]
        [Index("AK_UserIdAndCelebrationYear", 1, IsUnique = true)]
        public string UserId { get; set; }

        // the person whose birthday gift is being voted on
        [ForeignKey("UserId")]
        [InverseProperty("GiftPolls")]
        public virtual ApplicationUser User { get; set; }

        // the year of the birthday that is being voted on
        [Index("AK_UserIdAndCelebrationYear", 2, IsUnique = true)]
        public int CelebrationYear { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? EndedAt { get; set; }

        public virtual ICollection<GiftPollVote> GiftPollVotes { get; set; }
    }
}