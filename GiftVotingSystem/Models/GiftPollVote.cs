﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GiftVotingSystem.Models
{
    public class GiftPollVote
    {
        public int Id { get; set; }

        [Required]
        [Index("AK_UserIdAndGiftPollId", 1, IsUnique = true)]
        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }

        [Required]
        [Index("AK_UserIdAndGiftPollId", 2, IsUnique = true)]
        public int GiftPollId { get; set; }

        public virtual GiftPoll GiftPoll { get; set; }

        [Required]
        public int GiftId { get; set; }

        public virtual Gift Gift { get; set; }  
    }
}