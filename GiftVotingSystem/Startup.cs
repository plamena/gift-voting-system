﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GiftVotingSystem.Startup))]
namespace GiftVotingSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
