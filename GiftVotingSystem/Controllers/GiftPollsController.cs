﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GiftVotingSystem.Models;
using GiftVotingSystem.ViewModels;
using Microsoft.AspNet.Identity;
using System.Globalization;

namespace GiftVotingSystem.Controllers
{
    public class GiftPollsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: GiftPolls
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            var currentUserId = User.Identity.GetUserId();
            ViewBag.currentUserId = currentUserId;
            var giftPolls = db.GiftPolls.Where(g => g.UserId != currentUserId).Include(g => g.Initiator).Include(g => g.User);
            return View(giftPolls.ToList());
        }

        // GET: GiftPolls/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiftPoll giftPoll = db.GiftPolls.Find(id);
            if (giftPoll == null)
            {
                return HttpNotFound();
            }

            var vm = new GiftPollDetailsViewModel() { GiftPoll = giftPoll };

            var results = from vote in db.GiftPollVotes
                          join gift in db.Gifts on vote.GiftId equals gift.Id
                          where vote.GiftPollId == giftPoll.Id
                          group vote by gift.Name into votes
                          let count = votes.Count()
                          orderby count descending
                          select new GiftPollResult {
                              GiftId = votes.Select(v => v.GiftId).FirstOrDefault(),
                              GiftName = votes.Select(v => v.Gift.Name).FirstOrDefault(),
                              VotesCount = count,
                              Users = votes.Select(v => v.User.Name).ToList()
                          };

            vm.Results = results.ToList();

            return View(vm);
        }

        // GET: GiftPolls/Vote/5
        [Authorize]
        public ActionResult Vote(int id)
        {
            // users can vote only for active polls
            GiftPoll giftPoll = db.GiftPolls.Where(poll => poll.Id == id).Where(poll => poll.EndedAt.Equals(null)).FirstOrDefault();
            if (giftPoll == null)
            {
                return HttpNotFound();
            }

            var currentUserId = User.Identity.GetUserId();

            var vote = giftPoll.GiftPollVotes.Where(v => v.UserId == currentUserId).FirstOrDefault();

            var vm = new GiftPollVoteViewModel();
            vm.BirthdayPerson = giftPoll.User;
            vm.CelebrationYear = giftPoll.CelebrationYear;
            vm.GiftList = db.Gifts;
            if(vote != null)
            {
                vm.GiftId = vote.GiftId;
            }
           
            return View(vm);
        }

        // POST: GiftPolls/Vote
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Vote(int id, [Bind(Include = "GiftId")] GiftPollVoteViewModel giftPollVM)
        {
            // users can vote only for active polls
            GiftPoll giftPoll = db.GiftPolls.Where(poll => poll.Id == id).Where(poll => poll.EndedAt.Equals(null)).FirstOrDefault();

            if (giftPoll == null)
            {
                return HttpNotFound();
            }

            var currentUserId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                var vote = db.GiftPollVotes.Where(v => v.GiftPollId == giftPoll.Id).Where(v => v.UserId == currentUserId).FirstOrDefault();
                if(vote == null)
                {
                    var giftPollVote = new GiftPollVote() { UserId = currentUserId, GiftId = giftPollVM.GiftId, GiftPollId = id };
                    db.GiftPollVotes.Add(giftPollVote);
                }
                else
                {
                    vote.GiftId = giftPollVM.GiftId;
                    db.Entry(vote).State = EntityState.Modified;
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            giftPollVM.BirthdayPerson = giftPoll.User;
            giftPollVM.CelebrationYear = giftPoll.CelebrationYear;
            giftPollVM.GiftList = db.Gifts;
            return View(giftPollVM);
        }


        // GET: GiftPolls/Create
        [Authorize]
        public ActionResult Create()
        {
            var giftPollCreateVm = new GiftPollCreateViewModel();
            SetCreateViewModel(giftPollCreateVm);

            return View(giftPollCreateVm);
        }

        // POST: GiftPolls/Create
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,CelebrationYear")] GiftPollCreateViewModel giftPollVM)
        {
            SetCreateViewModel(giftPollVM);

            if (ModelState.IsValid)
            {
                var giftPoll = new GiftPoll() { InitiatorId = giftPollVM.InitiatorId, UserId = giftPollVM.UserId, CelebrationYear = giftPollVM.CelebrationYear, CreatedAt = giftPollVM.CreatedAt };
                db.GiftPolls.Add(giftPoll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(giftPollVM);
        }

        // GET: GiftPolls/Stop/5
        [Authorize]
        public ActionResult Stop(int id)
        {
            var currentUserId = User.Identity.GetUserId();
            GiftPoll giftPoll = db.GiftPolls.Where(p => p.InitiatorId == currentUserId).Where(p => p.Id == id).First();
            if (giftPoll == null)
            {
                return HttpNotFound();
            }
            return View(giftPoll);
        }

        // POST: GiftPolls/Stop/5
        [HttpPost, ActionName("Stop")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult StopPoll(int id)
        {
            GiftPoll giftPoll = db.GiftPolls.Find(id);
            if(giftPoll.EndedAt.Equals(null))
            {
                giftPoll.EndedAt = DateTime.Now;
            }
            
            db.Entry(giftPoll).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [AllowAnonymous]
        [HttpPost]
        public ActionResult CheckIfUserHasActivePoll(string UserId)
        {
            var usersWithActivePolls = from user in db.Users
                                       join poll in db.GiftPolls on user.Id equals poll.UserId
                                       where poll.EndedAt.Equals(null)
                                       select user;

            var userHasActivePoll = usersWithActivePolls.Any(u => u.Id == UserId);

            return Json(!userHasActivePoll, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult CheckIfYearIsTaken(string UserId, int CelebrationYear)
        {
            var takenYears = (from poll in db.GiftPolls
                              where poll.UserId == UserId
                              select poll.CelebrationYear);

            var yearIsTaken = takenYears.Any(year => year == CelebrationYear);

            return Json(!yearIsTaken, JsonRequestBehavior.AllowGet);
        }

        private void SetCreateViewModel(GiftPollCreateViewModel vm)
        {
            var currentUserId = User.Identity.GetUserId();

            // users that don't have an active gift poll and are not the initiator
            var validUsers = from user in db.Users
                             where !(from poll in db.GiftPolls
                                     where poll.EndedAt.Equals(null)
                                     select poll.UserId).Contains(user.Id)
                             where user.Id != currentUserId
                             select user;

         

            vm.UsersList = new SelectList(validUsers, "Id", "Name");
           // vm.UsersList = new SelectList(db.Users, "Id", "Name");
            vm.InitiatorId = currentUserId;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
