﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GiftVotingSystem.ViewModels
{
    public class GiftPollCreateViewModel : IValidatableObject
    {
        public GiftPollCreateViewModel()
        {
            CreatedAt = DateTime.Now;

            PossibleCelebrationYears = new List<SelectListItem>();

            var currentYear = DateTime.Now.Year;
            var maxCelebrations = 10;
   
            for (int i = 0; i < maxCelebrations; i++)
            {
                PossibleCelebrationYears.Add(new SelectListItem { Text = currentYear.ToString(), Value = currentYear.ToString() });
                currentYear++;
            }
        }

        public string InitiatorId { get; set; }

        [Required]
        [Remote("CheckIfUserHasActivePoll", "GiftPolls", HttpMethod = "POST", ErrorMessage = "The user already has an active poll.")]
        public string UserId { get; set; }

        [Required]
        [Remote("CheckIfYearIsTaken", "GiftPolls", AdditionalFields="UserId", HttpMethod = "POST", ErrorMessage = "This user already has a poll for this year.")]
        public int CelebrationYear { get; set; }

        public DateTime CreatedAt { get; set; }

        public List<SelectListItem> PossibleCelebrationYears { get; set; }
        public IEnumerable<SelectListItem> UsersList { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(UserId == InitiatorId)
            {
                yield return new ValidationResult
              ("You can't create a poll for yourself.", new[] { "UserId", "InitiatorId" });
            }
        }
    }
}