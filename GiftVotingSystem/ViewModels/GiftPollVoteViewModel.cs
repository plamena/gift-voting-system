﻿using GiftVotingSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GiftVotingSystem.ViewModels
{
    public class GiftPollVoteViewModel
    {
        public GiftPollVoteViewModel()
        {
            GiftList = new List<Gift>();
        }

        [Required]
        public int GiftPollId { get; set; }

        [Required]
        public int GiftId { get; set; }

        public ApplicationUser BirthdayPerson { get; set; }

        public int? CelebrationYear { get; set; }

        public IEnumerable<Gift> GiftList { get; set; }

        public string BirthdayPersonName
        {
            get
            {
                return BirthdayPerson?.Name;
            }   
        }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CelebrationDate
        {
            get
            {
                if ((BirthdayPerson != null) && (CelebrationYear != null))
                {
                    return new DateTime((int)CelebrationYear, BirthdayPerson.Birthdate.Month, BirthdayPerson.Birthdate.Day);
                }

                return null;
            }
        }
    }
}