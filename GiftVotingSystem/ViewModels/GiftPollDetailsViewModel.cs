﻿using GiftVotingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GiftVotingSystem.ViewModels
{
    public class GiftPollDetailsViewModel
    {
        public GiftPoll GiftPoll { get; set; }
        public List<GiftPollResult> Results { get; set; }
    }
}