namespace GiftVotingSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateGifts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Gifts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "GiftsNameIndex");

        }
        
        public override void Down()
        {
            DropTable("dbo.Gifts");
        }
    }
}
