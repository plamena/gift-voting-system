namespace GiftVotingSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateGiftPollVotes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GiftPollVotes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        GiftPollId = c.Int(nullable: false),
                        GiftId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gifts", t => t.GiftId, cascadeDelete: true)
                .ForeignKey("dbo.GiftPolls", t => t.GiftPollId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => new { t.UserId, t.GiftPollId }, unique: true, name: "AK_UserIdAndGiftPollId")
                .Index(t => t.GiftId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GiftPollVotes", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.GiftPollVotes", "GiftPollId", "dbo.GiftPolls");
            DropForeignKey("dbo.GiftPollVotes", "GiftId", "dbo.Gifts");
            DropIndex("dbo.GiftPollVotes", new[] { "GiftId" });
            DropIndex("dbo.GiftPollVotes", "AK_UserIdAndGiftPollId");
            DropTable("dbo.GiftPollVotes");
        }
    }
}
