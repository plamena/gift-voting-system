namespace GiftVotingSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUniquenessConstraintsToGiftPolls : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.GiftPolls", new[] { "UserId" });
            CreateIndex("dbo.GiftPolls", new[] { "UserId", "CelebrationYear" }, unique: true, name: "AK_UserIdAndCelebrationYear");
        }
        
        public override void Down()
        {
            DropIndex("dbo.GiftPolls", "AK_UserIdAndCelebrationYear");
            CreateIndex("dbo.GiftPolls", "UserId");
        }
    }
}
