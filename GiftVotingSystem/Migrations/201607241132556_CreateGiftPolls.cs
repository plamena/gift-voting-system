namespace GiftVotingSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateGiftPolls : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GiftPolls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InitiatorId = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        CelebrationYear = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        EndedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.InitiatorId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.InitiatorId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GiftPolls", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.GiftPolls", "InitiatorId", "dbo.AspNetUsers");
            DropIndex("dbo.GiftPolls", new[] { "UserId" });
            DropIndex("dbo.GiftPolls", new[] { "InitiatorId" });
            DropTable("dbo.GiftPolls");
        }
    }
}
