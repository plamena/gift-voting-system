namespace GiftVotingSystem.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "GiftVotingSystem.Models.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            SeedUsers(context);
            SeedGifts(context);
            SeedGiftPollAndVotes(context);

            //context.GiftPolls.AddOrUpdate(x => x.Id, 
            //       new GiftPoll() { Id = 1, InitiatorId = context.Users.First().Id, UserId = context.Users.First().Id, CelebrationYear = 2016, CreatedAt = DateTime.Now }
            //);
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var users = new ApplicationUser[] {
                                                 new ApplicationUser { UserName = "user1", Name = "Andrew Peters", Birthdate = new DateTime(1989, 1, 10) },
                                                 new ApplicationUser { UserName = "user2", Name = "Jane Jones", Birthdate = new DateTime(1980, 3, 30) },
                                                 new ApplicationUser { UserName = "user3", Name = "Lisa Smith", Birthdate = new DateTime(1971, 3, 30) },
                                                 new ApplicationUser { UserName = "user4", Name = "George Smith", Birthdate = new DateTime(1981, 3, 30) },
                                                 new ApplicationUser { UserName = "user5", Name = "Lisa Black", Birthdate = new DateTime(1961, 3, 30) },
                                                 new ApplicationUser { UserName = "user6", Name = "Jhon Black", Birthdate = new DateTime(1991, 6, 30) }
                                                };
            foreach (var user in users)
            {
                if (!(context.Users.Any(u => u.UserName == user.UserName)))
                {
                    userManager.Create(user, "Password@123");
                }
            }

            userManager.Dispose();
            userStore.Dispose();
        }

        private void SeedGifts(ApplicationDbContext context)
        {
            context.Gifts.AddOrUpdate(x => x.Id,
                new Gift() { Id = 1, Name = "Gift#1" },
                new Gift() { Id = 2, Name = "Gift#2" },
                new Gift() { Id = 3, Name = "Gift#3" },
                new Gift() { Id = 4, Name = "Gift#4" },
                new Gift() { Id = 5, Name = "Gift#5" }
            );
        }

        private void SeedGiftPollAndVotes(ApplicationDbContext context)
        {
            var users = context.Users.ToList();
            var giftPoll = new GiftPoll() { Id = 1, InitiatorId = users[0].Id, UserId = users[1].Id, CelebrationYear = 2016, CreatedAt = DateTime.Now };
            context.GiftPolls.AddOrUpdate(x => x.Id,
               giftPoll
            );

            context.GiftPollVotes.AddOrUpdate(x => x.Id,
                new GiftPollVote() { GiftPollId = giftPoll.Id, GiftId = 1, UserId = users[2].Id },
                new GiftPollVote() { GiftPollId = giftPoll.Id, GiftId = 1, UserId = users[3].Id },
                new GiftPollVote() { GiftPollId = giftPoll.Id, GiftId = 2, UserId = users[4].Id }
            );
        }
    }
}
